<?php   
// CSS
define('_CSS_',  '/plugin/AdminLTE/dist/css/');

//JS
define('_JS_',  '/plugin/AdminLTE/dist/js/');

//IMG
define('_IMG_',  '/plugin/AdminLTE/dist/img/');

//PLUGINS
define('_PLUGINS_',  '/plugin/AdminLTE/plugins/');
  